<?php


namespace Administracion\MinsalBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Incidente
 *
 * @ORM\Table(name="incidente", indexes={@ORM\Index(name="FK_PODRIA_TENER", columns={"ID_VEHICULO"})})
 * @ORM\Entity
 */
class Incidente
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID_INCIDENTE", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idIncidente;

    /**
     * @var string
     *
     * @ORM\Column(name="DETALLE", type="string", length=100, nullable=true)
     */
    private $detalle;

    /**
     * @var string
     *
     * @ORM\Column(name="LUGAR", type="string", length=50, nullable=true)
     */
    private $lugar;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="FECHA", type="datetime", nullable=true, columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
     */
    private $fecha;

    /**
     * @var \Vehiculo
     *
     * @ORM\ManyToOne(targetEntity="Vehiculo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_VEHICULO", referencedColumnName="ID_VEHICULO")
     * })
     */
    private $idVehiculo;



    /**
     * Get idIncidente
     *
     * @return integer 
     */
    public function getIdIncidente()
    {
        return $this->idIncidente;
    }

    /**
     * Set detalle
     *
     * @param string $detalle
     * @return Incidente
     */
    public function setDetalle($detalle)
    {
        $this->detalle = $detalle;

        return $this;
    }

    /**
     * Get detalle
     *
     * @return string 
     */
    public function getDetalle()
    {
        return $this->detalle;
    }

    /**
     * Set lugar
     *
     * @param string $lugar
     * @return Incidente
     */
    public function setLugar($lugar)
    {
        $this->lugar = $lugar;

        return $this;
    }

    /**
     * Get lugar
     *
     * @return string 
     */
    public function getLugar()
    {
        return $this->lugar;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     * @return Incidente
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get fecha
     *
     * @return \DateTime 
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set idVehiculo
     *
     * @param \Administracion\MinsalBundle\Entity\Vehiculo $idVehiculo
     * @return Incidente
     */
    public function setIdVehiculo(\Administracion\MinsalBundle\Entity\Vehiculo $idVehiculo = null)
    {
        $this->idVehiculo = $idVehiculo;

        return $this;
    }

    /**
     * Get idVehiculo
     *
     * @return \Administracion\MinsalBundle\Entity\Vehiculo 
     */
    public function getIdVehiculo()
    {
        return $this->idVehiculo;
    }
}
