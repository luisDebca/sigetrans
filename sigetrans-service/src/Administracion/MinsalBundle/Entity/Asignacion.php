<?php


namespace Administracion\MinsalBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use JMS\Serializer\Annotation\Type as JMS;


/**
 * Asignacion
 *
 * @ORM\Table(name="asignacion", indexes={@ORM\Index(name="FK_ASIGNADO", columns={"ID_CONDUCTOR"}), @ORM\Index(name="FK_RELATIONSHIP_2", columns={"ID_VEHICULO"}), @ORM\Index(name="FK_TIENE", columns={"ID_RUTA"})})
 * @ORM\Entity
 */
class Asignacion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID_ASIGNACION", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idAsignacion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="FECHA_INICIO", type="datetime", nullable=true, columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
     *
     */
    private $fechaInicio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="FECHA_FIN", type="datetime", nullable=true, columnDefinition="TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
     *
     */
    private $fechaFin;

    /**
     * @var string
     *
     * @ORM\Column(name="NOMBRE_SOLICITANTE", type="string", length=50, nullable=true)
     */
    private $nombreSolicitante;

    /**
     * @var string
     *
     * @ORM\Column(name="CARNET_SOLICITANTE", type="string", length=50, nullable=true)
     */
    private $carnetSolicitante;

    /**
     * @var \Conductor
     *
     * @ORM\ManyToOne(targetEntity="Conductor", cascade={"all"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_CONDUCTOR", referencedColumnName="ID_CONDUCTOR")
     * })
     */
    private $idConductor;

    /**
     * @var \Vehiculo
     *
     * @ORM\ManyToOne(targetEntity="Vehiculo", cascade={"all"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_VEHICULO", referencedColumnName="ID_VEHICULO")
     * })
     */
    private $idVehiculo;

    /**
     * @var \Ruta
     *
     * @ORM\ManyToOne(targetEntity="Ruta", cascade={"all"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_RUTA", referencedColumnName="ID_RUTA")
     * })
     */
    private $idRuta;



    /**
     * Get idAsignacion
     *
     * @return integer 
     */
    public function getIdAsignacion()
    {
        return $this->idAsignacion;
    }

    /**
     * Set fechaInicio
     *
     * @param \DateTime $fechaInicio
     * @return Asignacion
     */
    public function setFechaInicio($fechaInicio)
    {
        $this->fechaInicio = $fechaInicio;

        return $this;
    }

    /**
     * Get fechaInicio
     *
     * @return \DateTime 
     */
    public function getFechaInicio()
    {
        return $this->fechaInicio;
    }

    /**
     * Set fechaFin
     *
     * @param \DateTime $fechaFin
     * @return Asignacion
     */
    public function setFechaFin($fechaFin)
    {
        $this->fechaFin = $fechaFin;

        return $this;
    }

    /**
     * Get fechaFin
     *
     * @return \DateTime 
     */
    public function getFechaFin()
    {
        return $this->fechaFin;
    }

    /**
     * Set nombreSolicitante
     *
     * @param string $nombreSolicitante
     * @return Asignacion
     */
    public function setNombreSolicitante($nombreSolicitante)
    {
        $this->nombreSolicitante = $nombreSolicitante;

        return $this;
    }

    /**
     * Get nombreSolicitante
     *
     * @return string 
     */
    public function getNombreSolicitante()
    {
        return $this->nombreSolicitante;
    }

    /**
     * Set carnetSolicitante
     *
     * @param string $carnetSolicitante
     * @return Asignacion
     */
    public function setCarnetSolicitante($carnetSolicitante)
    {
        $this->carnetSolicitante = $carnetSolicitante;

        return $this;
    }

    /**
     * Get carnetSolicitante
     *
     * @return string 
     */
    public function getCarnetSolicitante()
    {
        return $this->carnetSolicitante;
    }

    /**
     * Set idConductor
     *
     * @param \Administracion\MinsalBundle\Entity\Conductor $idConductor
     * @return Asignacion
     */
    public function setIdConductor(\Administracion\MinsalBundle\Entity\Conductor $idConductor = null)
    {
        $this->idConductor = $idConductor;

        return $this;
    }

    /**
     * Get idConductor
     *
     * @return \Administracion\MinsalBundle\Entity\Conductor 
     */
    public function getIdConductor()
    {
        return $this->idConductor;
    }

    /**
     * Set idVehiculo
     *
     * @param \Administracion\MinsalBundle\Entity\Vehiculo $idVehiculo
     * @return Asignacion
     */
    public function setIdVehiculo(\Administracion\MinsalBundle\Entity\Vehiculo $idVehiculo = null)
    {
        $this->idVehiculo = $idVehiculo;

        return $this;
    }

    /**
     * Get idVehiculo
     *
     * @return \Administracion\MinsalBundle\Entity\Vehiculo 
     */
    public function getIdVehiculo()
    {
        return $this->idVehiculo;
    }

    /**
     * Set idRuta
     *
     * @param \Administracion\MinsalBundle\Entity\Ruta $idRuta
     * @return Asignacion
     */
    public function setIdRuta(\Administracion\MinsalBundle\Entity\Ruta $idRuta = null)
    {
        $this->idRuta = $idRuta;

        return $this;
    }

    /**
     * Get idRuta
     *
     * @return \Administracion\MinsalBundle\Entity\Ruta 
     */
    public function getIdRuta()
    {
        return $this->idRuta;
    }
}
