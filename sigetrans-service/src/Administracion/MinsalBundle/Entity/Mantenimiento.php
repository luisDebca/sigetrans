<?php


namespace Administracion\MinsalBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Mantenimiento
 *
 * @ORM\Table(name="mantenimiento", indexes={@ORM\Index(name="FK_ESTAR_EN", columns={"ID_VEHICULO"}), @ORM\Index(name="FK_TIENE_UN", columns={"ID_PROVEEDOR"})})
 * @ORM\Entity
 */
class Mantenimiento
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID_MANTENIMIENTO", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idMantenimiento;

    /**
     * @var string
     *
     * @ORM\Column(name="DIAGNOSTICO", type="string", length=100, nullable=true)
     */
    private $diagnostico;

    /**
     * @var string
     *
     * @ORM\Column(name="TIPO_MANTENIMIENTO", type="string", length=50, nullable=true)
     */
    private $tipoMantenimiento;

    /**
     * @var string
     *
     * @ORM\Column(name="TIPO_REVISION", type="string", length=50, nullable=true)
     */
    private $tipoRevision;

    /**
     * @var float
     *
     * @ORM\Column(name="COSTO", type="float", precision=10, scale=0, nullable=true)
     */
    private $costo;

    /**
     * @var string
     *
     * @ORM\Column(name="PERIODO_MANTENIMIENTO", type="string", length=30, nullable=true)
     */
    private $periodoMantenimiento;

    /**
     * @var \Vehiculo
     *
     * @ORM\ManyToOne(targetEntity="Vehiculo", cascade={"all"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_VEHICULO", referencedColumnName="ID_VEHICULO")
     * })
     */
    private $idVehiculo;

    /**
     * @var \Proveedor
     *
     * @ORM\ManyToOne(targetEntity="Proveedor", cascade={"all"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ID_PROVEEDOR", referencedColumnName="ID_PROVEEDOR")
     * })
     */
    private $idProveedor;



    /**
     * Get idMantenimiento
     *
     * @return integer 
     */
    public function getIdMantenimiento()
    {
        return $this->idMantenimiento;
    }

    /**
     * Set diagnostico
     *
     * @param string $diagnostico
     * @return Mantenimiento
     */
    public function setDiagnostico($diagnostico)
    {
        $this->diagnostico = $diagnostico;

        return $this;
    }

    /**
     * Get diagnostico
     *
     * @return string 
     */
    public function getDiagnostico()
    {
        return $this->diagnostico;
    }

    /**
     * Set tipoMantenimiento
     *
     * @param string $tipoMantenimiento
     * @return Mantenimiento
     */
    public function setTipoMantenimiento($tipoMantenimiento)
    {
        $this->tipoMantenimiento = $tipoMantenimiento;

        return $this;
    }

    /**
     * Get tipoMantenimiento
     *
     * @return string 
     */
    public function getTipoMantenimiento()
    {
        return $this->tipoMantenimiento;
    }

    /**
     * Set tipoRevision
     *
     * @param string $tipoRevision
     * @return Mantenimiento
     */
    public function setTipoRevision($tipoRevision)
    {
        $this->tipoRevision = $tipoRevision;

        return $this;
    }

    /**
     * Get tipoRevision
     *
     * @return string 
     */
    public function getTipoRevision()
    {
        return $this->tipoRevision;
    }

    /**
     * Set costo
     *
     * @param float $costo
     * @return Mantenimiento
     */
    public function setCosto($costo)
    {
        $this->costo = $costo;

        return $this;
    }

    /**
     * Get costo
     *
     * @return float 
     */
    public function getCosto()
    {
        return $this->costo;
    }

    /**
     * Set periodoMantenimiento
     *
     * @param string $periodoMantenimiento
     * @return Mantenimiento
     */
    public function setPeriodoMantenimiento($periodoMantenimiento)
    {
        $this->periodoMantenimiento = $periodoMantenimiento;

        return $this;
    }

    /**
     * Get periodoMantenimiento
     *
     * @return string 
     */
    public function getPeriodoMantenimiento()
    {
        return $this->periodoMantenimiento;
    }

    /**
     * Set idVehiculo
     *
     * @param \Administracion\MinsalBundle\Entity\Vehiculo $idVehiculo
     * @return Mantenimiento
     */
    public function setIdVehiculo(\Administracion\MinsalBundle\Entity\Vehiculo $idVehiculo = null)
    {
        $this->idVehiculo = $idVehiculo;

        return $this;
    }

    /**
     * Get idVehiculo
     *
     * @return \Administracion\MinsalBundle\Entity\Vehiculo 
     */
    public function getIdVehiculo()
    {
        return $this->idVehiculo;
    }

    /**
     * Set idProveedor
     *
     * @param \Administracion\MinsalBundle\Entity\Proveedor $idProveedor
     * @return Mantenimiento
     */
    public function setIdProveedor(\Administracion\MinsalBundle\Entity\Proveedor $idProveedor = null)
    {
        $this->idProveedor = $idProveedor;

        return $this;
    }

    /**
     * Get idProveedor
     *
     * @return \Administracion\MinsalBundle\Entity\Proveedor 
     */
    public function getIdProveedor()
    {
        return $this->idProveedor;
    }
}
