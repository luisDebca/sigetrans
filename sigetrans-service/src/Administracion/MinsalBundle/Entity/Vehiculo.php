<?php


namespace Administracion\MinsalBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Vehiculo
 *
 * @ORM\Table(name="vehiculo")
 * @ORM\Entity
 */
class Vehiculo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID_VEHICULO", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idVehiculo;

    /**
     * @var string
     *
     * @ORM\Column(name="NUMERO_PLACA", type="string", length=30, nullable=true)
     */
    private $numeroPlaca;

    /**
     * @var string
     *
     * @ORM\Column(name="TARJETA_CIRCULACION", type="string", length=30, nullable=true)
     */
    private $tarjetaCirculacion;

    /**
     * @var string
     *
     * @ORM\Column(name="TIPO", type="string", length=50, nullable=true)
     */
    private $tipo;

    /**
     * @var string
     *
     * @ORM\Column(name="COLOR", type="string", length=30, nullable=true)
     */
    private $color;

    /**
     * @var string
     *
     * @ORM\Column(name="MARCA", type="string", length=30, nullable=true)
     */
    private $marca;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Unidadorganizacional", mappedBy="idVehiculo", cascade={"all"})
     */
    private $idUnidad;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idUnidad = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get idVehiculo
     *
     * @return integer 
     */
    public function getIdVehiculo()
    {
        return $this->idVehiculo;
    }

    /**
     * Set numeroPlaca
     *
     * @param string $numeroPlaca
     * @return Vehiculo
     */
    public function setNumeroPlaca($numeroPlaca)
    {
        $this->numeroPlaca = $numeroPlaca;

        return $this;
    }

    /**
     * Get numeroPlaca
     *
     * @return string 
     */
    public function getNumeroPlaca()
    {
        return $this->numeroPlaca;
    }

    /**
     * Set tarjetaCirculacion
     *
     * @param string $tarjetaCirculacion
     * @return Vehiculo
     */
    public function setTarjetaCirculacion($tarjetaCirculacion)
    {
        $this->tarjetaCirculacion = $tarjetaCirculacion;

        return $this;
    }

    /**
     * Get tarjetaCirculacion
     *
     * @return string 
     */
    public function getTarjetaCirculacion()
    {
        return $this->tarjetaCirculacion;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     * @return Vehiculo
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string 
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * Set color
     *
     * @param string $color
     * @return Vehiculo
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string 
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set marca
     *
     * @param string $marca
     * @return Vehiculo
     */
    public function setMarca($marca)
    {
        $this->marca = $marca;

        return $this;
    }

    /**
     * Get marca
     *
     * @return string 
     */
    public function getMarca()
    {
        return $this->marca;
    }

    /**
     * Add idUnidad
     *
     * @param \Administracion\MinsalBundle\Entity\Unidadorganizacional $idUnidad
     * @return Vehiculo
     */
    public function addIdUnidad(\Administracion\MinsalBundle\Entity\Unidadorganizacional $idUnidad)
    {
        $this->idUnidad[] = $idUnidad;

        return $this;
    }

    /**
     * Remove idUnidad
     *
     * @param \Administracion\MinsalBundle\Entity\Unidadorganizacional $idUnidad
     */
    public function removeIdUnidad(\Administracion\MinsalBundle\Entity\Unidadorganizacional $idUnidad)
    {
        $this->idUnidad->removeElement($idUnidad);
    }

    /**
     * Get idUnidad
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIdUnidad()
    {
        return $this->idUnidad;
    }
}
