<?php


namespace Administracion\MinsalBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Conductor
 *
 * @ORM\Table(name="conductor", indexes={@ORM\Index(name="FK_ASIGNADO_UN", columns={"ID_RECURSO"})})
 * @ORM\Entity
 */
class Conductor
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID_CONDUCTOR", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idConductor;

    /**
     * @var string
     *
     * @ORM\Column(name="NOMBRE", type="string", length=40, nullable=true)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="NIT", type="string", length=30, nullable=true)
     */
    private $nit;

    /**
     * @var string
     *
     * @ORM\Column(name="TELEFONO", type="string", length=30, nullable=true)
     */
    private $telefono;

    /**
     * @var \Recurso
     *
     * @ORM\OneToOne(targetEntity="Recurso", cascade={"all"})
     * @ORM\JoinColumn(name="ID_RECURSO", referencedColumnName="ID_RECURSO")
     *
     */
    private $idRecurso;



    /**
     * Get idConductor
     *
     * @return integer 
     */
    public function getIdConductor()
    {
        return $this->idConductor;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Conductor
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set nit
     *
     * @param string $nit
     * @return Conductor
     */
    public function setNit($nit)
    {
        $this->nit = $nit;

        return $this;
    }

    /**
     * Get nit
     *
     * @return string 
     */
    public function getNit()
    {
        return $this->nit;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     * @return Conductor
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string 
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set idRecurso
     *
     * @param \Administracion\MinsalBundle\Entity\Recurso $idRecurso
     * @return Conductor
     */
    public function setIdRecurso(\Administracion\MinsalBundle\Entity\Recurso $idRecurso = null)
    {
        $this->idRecurso = $idRecurso;

        return $this;
    }

    /**
     * Get idRecurso
     *
     * @return \Administracion\MinsalBundle\Entity\Recurso 
     */
    public function getIdRecurso()
    {
        return $this->idRecurso;
    }
}
