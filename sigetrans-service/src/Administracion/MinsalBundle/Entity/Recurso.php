<?php


namespace Administracion\MinsalBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Recurso
 *
 * @ORM\Table(name="recurso")
 * @ORM\Entity
 */
class Recurso
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID_RECURSO", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idRecurso;

    /**
     * @var string
     *
     * @ORM\Column(name="VALE_GASOLINA", type="string", length=30, nullable=true)
     */
    private $valeGasolina;

    /**
     * @var string
     *
     * @ORM\Column(name="VIATICOS", type="string", length=30, nullable=true)
     */
    private $viaticos;



    /**
     * Get idRecurso
     *
     * @return integer 
     */
    public function getIdRecurso()
    {
        return $this->idRecurso;
    }

    /**
     * Set valeGasolina
     *
     * @param string $valeGasolina
     * @return Recurso
     */
    public function setValeGasolina($valeGasolina)
    {
        $this->valeGasolina = $valeGasolina;

        return $this;
    }

    /**
     * Get valeGasolina
     *
     * @return string 
     */
    public function getValeGasolina()
    {
        return $this->valeGasolina;
    }

    /**
     * Set viaticos
     *
     * @param string $viaticos
     * @return Recurso
     */
    public function setViaticos($viaticos)
    {
        $this->viaticos = $viaticos;

        return $this;
    }

    /**
     * Get viaticos
     *
     * @return string 
     */
    public function getViaticos()
    {
        return $this->viaticos;
    }
}
