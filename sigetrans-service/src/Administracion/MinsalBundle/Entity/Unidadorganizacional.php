<?php


namespace Administracion\MinsalBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Unidadorganizacional
 *
 * @ORM\Table(name="unidadorganizacional")
 * @ORM\Entity
 */
class Unidadorganizacional
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID_UNIDAD", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idUnidad;

    /**
     * @var string
     *
     * @ORM\Column(name="NOMBRE", type="string", length=40, nullable=true)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="DIRECCION", type="string", length=100, nullable=true)
     */
    private $direccion;

    /**
     * @var string
     *
     * @ORM\Column(name="DEPARTAMENTO", type="string", length=50, nullable=true)
     */
    private $departamento;

    /**
     * @var string
     *
     * @ORM\Column(name="MUNICIPIO", type="string", length=50, nullable=true)
     */
    private $municipio;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Vehiculo", inversedBy="idUnidad", cascade={"all"})
     * @ORM\JoinTable(name="unidad_vehiculo",
     *   joinColumns={
     *     @ORM\JoinColumn(name="ID_UNIDAD", referencedColumnName="ID_UNIDAD")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="ID_VEHICULO", referencedColumnName="ID_VEHICULO")
     *   }
     * )
     */
    private $idVehiculo;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->idVehiculo = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get idUnidad
     *
     * @return integer 
     */
    public function getIdUnidad()
    {
        return $this->idUnidad;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Unidadorganizacional
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     * @return Unidadorganizacional
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get direccion
     *
     * @return string 
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set departamento
     *
     * @param string $departamento
     * @return Unidadorganizacional
     */
    public function setDepartamento($departamento)
    {
        $this->departamento = $departamento;

        return $this;
    }

    /**
     * Get departamento
     *
     * @return string 
     */
    public function getDepartamento()
    {
        return $this->departamento;
    }

    /**
     * Set municipio
     *
     * @param string $municipio
     * @return Unidadorganizacional
     */
    public function setMunicipio($municipio)
    {
        $this->municipio = $municipio;

        return $this;
    }

    /**
     * Get municipio
     *
     * @return string 
     */
    public function getMunicipio()
    {
        return $this->municipio;
    }

    /**
     * Add idVehiculo
     *
     * @param \Administracion\MinsalBundle\Entity\Vehiculo $idVehiculo
     * @return Unidadorganizacional
     */
    public function addIdVehiculo(\Administracion\MinsalBundle\Entity\Vehiculo $idVehiculo)
    {
        $this->idVehiculo[] = $idVehiculo;

        return $this;
    }

    /**
     * Remove idVehiculo
     *
     * @param \Administracion\MinsalBundle\Entity\Vehiculo $idVehiculo
     */
    public function removeIdVehiculo(\Administracion\MinsalBundle\Entity\Vehiculo $idVehiculo)
    {
        $this->idVehiculo->removeElement($idVehiculo);
    }

    /**
     * Get idVehiculo
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIdVehiculo()
    {
        return $this->idVehiculo;
    }
}
