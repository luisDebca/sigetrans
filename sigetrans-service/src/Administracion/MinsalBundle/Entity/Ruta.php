<?php


namespace Administracion\MinsalBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * Ruta
 *
 * @ORM\Table(name="ruta")
 * @ORM\Entity
 */
class Ruta
{
    /**
     * @var integer
     *
     * @ORM\Column(name="ID_RUTA", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idRuta;

    /**
     * @var string
     *
     * @ORM\Column(name="DISTANCIA", type="string", length=30, nullable=true)
     */
    private $distancia;

    /**
     * @var string
     *
     * @ORM\Column(name="INICIO", type="string", length=30, nullable=true)
     */
    private $inicio;

    /**
     * @var string
     *
     * @ORM\Column(name="DESTINO", type="string", length=30, nullable=true)
     */
    private $destino;

    /**
     * @var string
     *
     * @ORM\Column(name="DEPARTAMENTO", type="string", length=50, nullable=true)
     */
    private $departamento;

    /**
     * @var string
     *
     * @ORM\Column(name="MUNICIPIO", type="string", length=50, nullable=true)
     */
    private $municipio;



    /**
     * Get idRuta
     *
     * @return integer 
     */
    public function getIdRuta()
    {
        return $this->idRuta;
    }

    /**
     * Set distancia
     *
     * @param string $distancia
     * @return Ruta
     */
    public function setDistancia($distancia)
    {
        $this->distancia = $distancia;

        return $this;
    }

    /**
     * Get distancia
     *
     * @return string 
     */
    public function getDistancia()
    {
        return $this->distancia;
    }

    /**
     * Set inicio
     *
     * @param string $inicio
     * @return Ruta
     */
    public function setInicio($inicio)
    {
        $this->inicio = $inicio;

        return $this;
    }

    /**
     * Get inicio
     *
     * @return string 
     */
    public function getInicio()
    {
        return $this->inicio;
    }

    /**
     * Set destino
     *
     * @param string $destino
     * @return Ruta
     */
    public function setDestino($destino)
    {
        $this->destino = $destino;

        return $this;
    }

    /**
     * Get destino
     *
     * @return string 
     */
    public function getDestino()
    {
        return $this->destino;
    }

    /**
     * Set departamento
     *
     * @param string $departamento
     * @return Ruta
     */
    public function setDepartamento($departamento)
    {
        $this->departamento = $departamento;

        return $this;
    }

    /**
     * Get departamento
     *
     * @return string 
     */
    public function getDepartamento()
    {
        return $this->departamento;
    }

    /**
     * Set municipio
     *
     * @param string $municipio
     * @return Ruta
     */
    public function setMunicipio($municipio)
    {
        $this->municipio = $municipio;

        return $this;
    }

    /**
     * Get municipio
     *
     * @return string 
     */
    public function getMunicipio()
    {
        return $this->municipio;
    }
}
