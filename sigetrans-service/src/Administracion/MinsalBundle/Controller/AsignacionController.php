<?php
/**
 * Created by PhpStorm.
 * User: kirio
 * Date: 28/11/2017
 * Time: 8:50 PM
 */

namespace Administracion\MinsalBundle\Controller;

use Administracion\MinsalBundle\Entity\Asignacion;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Administracion\MinsalBundle\Entity\Conductor;
use Administracion\MinsalBundle\Entity\Vehiculo;
use Symfony\Component\HttpFoundation\Response;

class AsignacionController extends FOSRestController
{
    /**
     * Lista de proveedores
     *
     * @Get("/asignaciones")
     */
    public function getAsignacionesAction(){

        $pro = $this->getDoctrine()->getRepository("AdministracionMinsalBundle:Asignacion")
            ->findAll();

        return new Response($this->get('jms_serializer')->serialize($pro, 'json'));
    }
    /**
     * Crear un nueva asignacion
     * @var Request $request
     *
     * @Post("/asignacion")
     */
    public function postAsignacionAction(Request $asignacion)
    {

        $prov = $this->get('jms_serializer')->deserialize($asignacion->getContent(), 'Administracion\MinsalBundle\Entity\Asignacion', 'json');

        $prov->setIdConductor($this->getDoctrine()->getRepository("AdministracionMinsalBundle:Conductor")
            ->find($prov->getIdConductor()));
        $prov->setIdVehiculo($this->getDoctrine()->getRepository("AdministracionMinsalBundle:Vehiculo")
            ->find($prov->getIdVehiculo()));
        $em = $this->getDoctrine()->getManager();
        $em->persist($prov);
        $em->flush();

        return new Response('se guardo correctamente',201);
        //return new Response($this->get('jms_serializer')->serialize($prov, 'json'));
    }

}