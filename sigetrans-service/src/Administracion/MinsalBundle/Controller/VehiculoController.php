<?php
namespace Administracion\MinsalBundle\Controller;

use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Administracion\MinsalBundle\Entity\Vehiculo;
use Symfony\Component\HttpFoundation\Response;


class VehiculoController extends FOSRestController
{

    /**
     * Lista de proveedores
     *
     * @Get("/vehiculos")
     */
    public function getVehiculosAction(){

        $prov = $this->getDoctrine()->getRepository("AdministracionMinsalBundle:Vehiculo")
            ->findAll();

        return new Response($this->get('jms_serializer')->serialize($prov, 'json'));
    }
}