<?php
/**
 * Created by PhpStorm.
 * User: kirio
 * Date: 30/11/2017
 * Time: 5:10 PM
 */

namespace Administracion\MinsalBundle\Controller;
use Administracion\MinsalBundle\Entity\Asignacion;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Administracion\MinsalBundle\Entity\Mantenimiento;
use Administracion\MinsalBundle\Entity\Proveedor;
use Symfony\Component\HttpFoundation\Response;


class MantenimientoController extends  FOSRestController
{
    /**
     * Crear un nuevo mantenimiento
     * @var Request $mantenimiento
     *
     * @Post("/mantenimiento")
     */
    public function postMantenimientoAction(Request $mantenimiento)
    {

        $man = $this->get('jms_serializer')->deserialize($mantenimiento->getContent(),
         'Administracion\MinsalBundle\Entity\Mantenimiento', 'json');

        $man->setIdProveedor($man->getIdProveedor());
        $man->setIdVehiculo($this->getDoctrine()->getRepository("AdministracionMinsalBundle:Vehiculo")->find($man->getIdVehiculo()));
        $em = $this->getDoctrine()->getManager();
        $em->persist($man);
        $em->flush();

        return new Response('se guardo correctamente',201);
        //return new Response($this->get('jms_serializer')->serialize($prov, 'json'));
    }

    /**
     * Lista de los mantenimientos
     *
     * @Get("/mantenimientos")
     */
    public function getMantenimientosAction(){

        $mantenimientos = $this->getDoctrine()->getRepository("AdministracionMinsalBundle:Mantenimiento")
            ->findAll();

        return new Response($this->get('jms_serializer')->serialize($mantenimientos, 'json'));
    }

    /**
     *
     * Eliminar un registro de mantenimiento
     * @var Mantenimiento $mantenimiento
     *
     *
     *
     * @ParamConverter("mantenimiento", class="AdministracionMinsalBundle:Mantenimiento")
     * @Delete("/mantenimiento/{id}")
     */
    public function deleteMantenimientoAction(Mantenimiento $mantenimiento)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($mantenimiento);
        $em->flush();
        return new Response('se elimino correctamente',201);
    }

    /**
     * editar
     *
     * @var Mantenimiento $mantenimiento
     *
     * @ParamConverter("mantenimiento", class="AdministracionMinsalBundle:Mantenimiento")
     * @Put("/editarMantenimiento/{id}")
     */
    public function putProveedorAction(Mantenimiento $mantenimiento)
    {
        $em = $this->getDoctrine()->getManager();
        $em->persist($mantenimiento);
        $em->flush();
        return new Response('se elimino correctamente',201);
    }
}