<?php
namespace Administracion\MinsalBundle\Controller;

use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Administracion\MinsalBundle\Entity\Proveedor;
use Administracion\MinsalBundle\Form\ProveedorType;
use Symfony\Component\HttpFoundation\Response;


class ProveedorController extends FOSRestController
{

    /**
     * Lista de proveedores
     *
     * @Get("/proveedores")
     */
    public function getProveedoresAction(){

        $proveedores = $this->getDoctrine()->getRepository("AdministracionMinsalBundle:Proveedor")
            ->findAll();

        return new Response($this->get('jms_serializer')->serialize($proveedores, 'json'));
    }

    /**
     * buscar un provedor por id
     * @param Proveedor $proveedor
     *
     *
     *
     * @ParamConverter("proveedor", class="AdministracionMinsalBundle:Proveedor")
     * @Get("/proveedor/{id}",)
     */
    public function getProveedorAction(Proveedor $proveedor){

        $view = $this->view($proveedor);
        return $this->handleView($view);

    }

    /**
     * Crear un nuevo proveedor
     * @var Request $request
     *
     * @Post("/proveedor")
     */
    public function postProveedorAction(Request $request)
    {

        $prov = $this->get('jms_serializer')->deserialize($request->getContent(), 'Administracion\MinsalBundle\Entity\Proveedor', 'json');
        $em = $this->getDoctrine()->getManager();
        $em->persist($prov);
        $em->flush();

        return new Response('se guardo correctamente',201);
        //return new Response($this->get('jms_serializer')->serialize($prov, 'json'));
    }

    /**
     * 
     * editar un proveedor
     * 
     * @var Proveedor $proveedor
     * 
     *
     *
     * @ParamConverter("proveedor", class="AdministracionMinsalBundle:Proveedor")
     * @Put("/proveedor/{id}")
     */
    public function putProveedorAction(Proveedor $proveedor)
    {
      
            $em = $this->getDoctrine()->getManager();

            $em->persist($proveedor);
            $em->flush();
         
            $view = $this->view($proveedor);
            return $this->handleView($view);
        

    }

    /**
     * 
     * Eliminar un proveedor
     * @var Proveedor $proveedor
     * 
     *
     * 
     * @ParamConverter("proveedor", class="AdministracionMinsalBundle:Proveedor")
     * @Delete("/proveedor/{id}")
     */
    public function deleteProveedorAction(Proveedor $proveedor)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($proveedor);
        $em->flush();
        $mensaje= array("Estado" => "Proveedor Elminado");
        $view = $this->view($mensaje);
        return $this->handleView($view);
    }

}