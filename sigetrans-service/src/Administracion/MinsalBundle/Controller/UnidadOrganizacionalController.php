<?php


namespace Administracion\MinsalBundle\Controller;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Administracion\MinsalBundle\Entity\Unidadorganizacional;

use Symfony\Component\HttpFoundation\Response;


class UnidadOrganizacionalController extends FOSRestController
{

     /**
     * Lista de unidades
     *
     * @Get("/unidades")
     */
    public function getUnidadorganizacionalesAction()
    {
        
     $unidadorganizacional = $this->getDoctrine()->getRepository("AdministracionMinsalBundle:Unidadorganizacional")
     ->findAll();
        
     $view = $this->view($unidadorganizacional);
     return $this->handleView($view);
    }
        
            /**
             * buscar un ruta por id
             * @param Unidadorganizacional $unidad
             *
             *
             *
             * @ParamConverter("unidad", class="AdministracionMinsalBundle:Unidadorganizacional")
             * @Get("/unidad/{id}",)
             */
            public function getUnidadorganizacionalAction(Unidadorganizacional $unidad)
            {

                $view = $this->view($unidad);
                return $this->handleView($view);
        
            }
        
            /**
             * Crear un nuevo conductor
             * @var Request $request
             *
             * @Post("/unidad")
             */
            public function postUnidadorganizacionalAction(Request $request)
            {
        
                $unidadNueva = $this->get('jms_serializer')->deserialize($request->getContent(), 'Administracion\MinsalBundle\Entity\Unidadorganizacional', 'json');
                $em = $this->getDoctrine()->getManager();
                $em->persist($unidadNueva);
                $em->flush();
                //return new Response($this->get('jms_serializer')->serialize($unidadNueva, 'json'));
                return new Response('se guardo correctamente',201);
            }
 /**
     * 
     * editar un proveedor
     * 
     * @var @param Unidadorganizacional $unidad
     * 
     * 
     *
     *
     * @ParamConverter("unidad", class="AdministracionMinsalBundle:Ruta")
     * @Put("/unidad/{id}")
     */
    public function putUnidadorganizacionalAction(Unidadorganizacional $unidad)
    {
      
            $em = $this->getDoctrine()->getManager();

            $em->persist($unidad);
            $em->flush();
         
            $view = $this->view($unidad);
            return $this->handleView($view);
        

    }

    /**
     * 
     * Eliminar una unidad
     * @var @param Unidadorganizacional $unidad
     * 
     *
     *
     * @ParamConverter("unidad", class="AdministracionMinsalBundle:Unidadorganizacional")
     * @Delete("/unidad/{id}")
     */
    public function deleteRutaAction(Unidadorganizacional $unidad)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($unidad);
        $em->flush();
        $mensaje= array("Estado" => "Unidad Elminada");
        $view = $this->view($mensaje);
        return $this->handleView($view);
    }
}