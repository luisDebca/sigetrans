<?php
namespace Administracion\MinsalBundle\Controller;

use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Administracion\MinsalBundle\Entity\Conductor;
use Symfony\Component\HttpFoundation\Response;


class ConductorController extends FOSRestController
{

    /**
     * Lista de conductores
     *
     * @Get("/conductores")
     */
    public function getConductoresAction(){

        $provee = $this->getDoctrine()->getRepository("AdministracionMinsalBundle:Conductor")
            ->findAll();

        return new Response($this->get('jms_serializer')->serialize($provee, 'json'));
    }

    /**
     * Crear un nuevo conductor
     * @var Request $conductor
     *
     * @Post("/conductor")
     */
    public function postConductorAction(Request $conductor)
    {

        $cond = $this->get('jms_serializer')->deserialize($conductor->getContent(),
         'Administracion\MinsalBundle\Entity\Conductor', 'json');

        //$man->setIdProveedor($man->getIdProveedor());
        //$man->setIdVehiculo($this->getDoctrine()->getRepository("AdministracionMinsalBundle:Vehiculo")->find($man->getIdVehiculo()));
        $em = $this->getDoctrine()->getManager();
        $em->persist($cond);
        $em->flush();

        return new Response('se guardo correctamente',201);
        //return new Response($this->get('jms_serializer')->serialize($prov, 'json'));
    }
}