<?php


namespace Administracion\MinsalBundle\Controller;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Administracion\MinsalBundle\Entity\Ruta;

use Symfony\Component\HttpFoundation\Response;


class RutaController extends FOSRestController
{

     /**
     * Lista de rutas
     *
     * @Get("/rutas")
     */
    public function getRutasAction()
    {
        
     $rutas = $this->getDoctrine()->getRepository("AdministracionMinsalBundle:Ruta")
     ->findAll();
        
     $view = $this->view($rutas);
     return $this->handleView($view);
    }
        
            /**
             * buscar un ruta por id
             * @param Ruta $ruta
             *
             *
             *
             * @ParamConverter("ruta", class="AdministracionMinsalBundle:Ruta")
             * @Get("/ruta/{id}",)
             */
            public function getRutaAction(Ruta $ruta)
            {

                $view = $this->view($ruta);
                return $this->handleView($view);
        
            }
        
            /**
             * Crear un nuevo conductor
             * @var Request $request
             *
             * @Post("/ruta")
             */
            public function postRutaAction(Request $request)
            {
        
                $rutaNueva = $this->get('jms_serializer')->deserialize($request->getContent(), 'Administracion\MinsalBundle\Entity\Ruta', 'json');
                $em = $this->getDoctrine()->getManager();
                $em->persist($rutaNueva);
                $em->flush();
                //return new Response($this->get('jms_serializer')->serialize($rutaNueva, 'json'));
                return new Response('se guardo correctamente',201);
            }
 /**
     * 
     * editar un proveedor
     * 
     * @var @param Ruta $ruta
     * 
     *
     *
     * @ParamConverter("ruta", class="AdministracionMinsalBundle:Ruta")
     * @Put("/ruta/{id}")
     */
    public function putRutaAction(Ruta $ruta)
    {
      
            $em = $this->getDoctrine()->getManager();

            $em->persist($ruta);
            $em->flush();
         
            $view = $this->view($ruta);
            return $this->handleView($view);
        

    }

    /**
     * 
     * Eliminar un proveedor
     * @var @param Ruta $ruta
     * 
     *
     *
     * @ParamConverter("ruta", class="AdministracionMinsalBundle:Ruta")
     * @Delete("/ruta/{id}")
     */
    public function deleteRutaAction(Ruta $ruta)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($ruta);
        $em->flush();
        $mensaje= array("Estado" => "Proveedor Elminado");
        $view = $this->view($mensaje);
        return $this->handleView($view);
    }
}