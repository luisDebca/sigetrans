<?php
namespace Administracion\MinsalBundle\Controller;

use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Put;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Administracion\MinsalBundle\Entity\Incidente;
use Administracion\MinsalBundle\Entity\Vehiculo;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\DateTime;


class IncidenteController extends FOSRestController
{

    /**
     * Lista de incidentes
     *
     * @Get("/incidentes")
     */
    public function getIncidentesAction(){

        $inci = $this->getDoctrine()->getRepository("AdministracionMinsalBundle:Incidente")
            ->findAll();

        return new Response($this->get('jms_serializer')->serialize($inci, 'json'));
    }

    /**
     * Crear un nuevo incidente
     * @var Request $incidente
     *
     * @Post("/incidente")
     */
    public function postIncidenteAction(Request $incidente)
    {

        $inc = $this->get('jms_serializer')->deserialize($incidente->getContent(), 'Administracion\MinsalBundle\Entity\Incidente', 'json');

        //$fecha = $incidente->request->get('fecha');
        //$inc->setFecha(new \DateTime($fecha));

        $inc->setIdVehiculo($this->getDoctrine()->getRepository("AdministracionMinsalBundle:Vehiculo")
            ->find($inc->getIdVehiculo()));
        $em = $this->getDoctrine()->getManager();
        $em->persist($inc);
        $em->flush();

        return new Response('se guardo correctamente',201);
        //return new Response($this->get('jms_serializer')->serialize($prov, 'json'));
    }

}
