<?php

namespace Administracion\MinsalBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('AdministracionMinsalBundle:Default:index.html.twig', array('name' => $name));
    }
}
