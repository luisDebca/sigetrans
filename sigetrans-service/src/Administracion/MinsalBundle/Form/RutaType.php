<?php

namespace Administracion\MinsalBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RutaType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('distancia')
            ->add('inicio')
            ->add('destino')
            ->add('departamento')
            ->add('municipio')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Administracion\MinsalBundle\Entity\Ruta'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'administracion_minsalbundle_ruta';
    }
}
