import { Component, OnInit } from '@angular/core';
import {Http, Response, Headers, RequestOptions}from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {Observer} from 'rxjs/Observer';

@Component({
  selector: 'app-conductores',
  templateUrl: './conductores.component.html',
  styleUrls: ['./conductores.component.css']
})
export class ConductoresComponent implements OnInit {

  baseURI:String = "http://127.0.0.1:8000/sigetrans/";
  listaConductores: Array<any>;

  constructor(private http:Http) { }

  ngOnInit() {
    this.getListaConductores();
  }

  getListaConductores():void{
    this.http.request(this.baseURI + "conductores")
    .subscribe((respuesta:Response) => this.listaConductores = respuesta.json())
  }

}
