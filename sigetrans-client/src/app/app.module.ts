import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { PdfmakeModule } from 'ng-pdf-make';

import {app_routing} from "./app.routes";

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { InicioComponent } from './inicio/inicio.component';
import { AsignacionComponent } from './asignacion/asignacion.component';
import { RegistrosComponent } from './registros/registros.component';
import {AlertCenterModule} from './modules/alert-center/alert-center.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MantenimientoComponent } from 'app/mantenimiento/mantenimiento.component';
import { NuevomantenimientoComponent } from 'app/nuevomantenimiento/nuevomantenimiento.component';
import { ConductoresComponent } from 'app/conductores/conductores.component';
import { ReportesComponent } from 'app/reportes/reportes.component';
import { ProveedoresComponent } from 'app/proveedores/proveedores.component';
import { ConductoresregistComponent } from 'app/conductoresregist/conductoresregist.component';
import { IncidentesComponent } from 'app/incidentes/incidentes.component';
import { IncidentesregistComponent } from 'app/incidentesregist/incidentesregist.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    InicioComponent,
    AsignacionComponent,
    RegistrosComponent,
    MantenimientoComponent,
    NuevomantenimientoComponent,
    ConductoresComponent,
    ReportesComponent,
    ProveedoresComponent,
    ConductoresregistComponent,
    IncidentesComponent,
    IncidentesregistComponent


  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    app_routing,
    AlertCenterModule,
    BrowserAnimationsModule,
    PdfmakeModule 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
