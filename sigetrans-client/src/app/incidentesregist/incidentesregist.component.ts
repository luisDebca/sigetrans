import { Component, OnInit } from '@angular/core';
import {Http, Response, Headers, RequestOptions}from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {Observer} from 'rxjs/Observer';
import {AlertCenterService} from '../modules';
import {AlertType} from '../modules';
import {Alert} from '../modules';

@Component({
  selector: 'app-incidentesregist',
  templateUrl: './incidentesregist.component.html',
  styleUrls: ['./incidentesregist.component.css']
})
export class IncidentesregistComponent implements OnInit {
  baseURI:String = "http://127.0.0.1:8000/sigetrans/";
  vehiculos:Array<any>;
  asignacion:Array<any>;
  incidenteItem: any;
  showAtt: Boolean;

  constructor(private http:Http,private service: AlertCenterService) { }

  ngOnInit() {
    this.incidenteItem={};
    this.obtenerTodosVehiculos();
  }

  obtenerTodosVehiculos():void{
  	this.http.request(this.baseURI+"vehiculos")
  	.subscribe((resp:Response)=>{
  		this.vehiculos = resp.json();
  	});
  }

  guardarIncidente():void{
  const alert = Alert.create(AlertType.SUCCESS, 'Se guardo correctamente', 5000);
  var headers = new Headers({'Content-Type':'application/json'})
  var options = new RequestOptions({headers: headers});

  var incidente = JSON.stringify(
                  {
                    detalle:  this.incidenteItem.detalle,
                    lugar: this.incidenteItem.lugar,
                    //fecha: this.incidenteItem.fecha,
                    id_vehiculo:
                    {
                      id_vehiculo: this.incidenteItem.idVehiculo
                    }
  })

      this.http.post(this.baseURI+"incidente",incidente,options)
      .subscribe((resp:Response)=>{
        this.service.alert(alert);
      });


    }

}
