import { Component, OnInit } from '@angular/core';
import {Http, Response, Headers, RequestOptions}from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {Observer} from 'rxjs/Observer';


@Component({
  selector: 'app-mantenimiento',
  templateUrl: './mantenimiento.component.html',
  styleUrls: ['./mantenimiento.component.css']
})
export class MantenimientoComponent implements OnInit {
  baseURI:String = "http://127.0.0.1:8000/sigetrans/";
  listaMantenimientos: Array<any>;

  constructor(private http:Http) { }

  ngOnInit() {
    this.getListaMantenimientos();
  }

  getListaMantenimientos():void{
    this.http.request(this.baseURI + "mantenimientos")
    .subscribe((respuesta:Response) => this.listaMantenimientos = respuesta.json())
  }
}
