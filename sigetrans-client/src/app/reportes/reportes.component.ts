import { Component, OnInit, Inject, ViewChild, ElementRef } from '@angular/core';
import {Http, Response, Headers, RequestOptions}from '@angular/http';
import { forEach } from '@angular/router/src/utils/collection';
import * as jsPDF from 'jspdf'

@Component({
  selector: 'app-reportes',
  templateUrl: './reportes.component.html',
  styleUrls: ['./reportes.component.css'],
  providers: [
    { provide: 'Window',  useValue: window }
  ]
})
export class ReportesComponent implements OnInit {
  listProveedores:Array<any>;
  listConductores:Array<any>;
  listVehiculos: Array<any>;
  baseURI:String = "http://127.0.0.1:8000/sigetrans/";
  constructor( @Inject('Window') private window: Window,private http:Http
) { }

  ngOnInit() {
    this.getListaDeProveedores();
    this.getListaDeConductores();
    this.getListaDeVehiculos();
  }
  proveedores() {
            let doc = new jsPDF();
            doc.text(10,62,"nombre");
            doc.text(60,62,"telefono");
            let altura:number=74;
            let alturaLine:number=78;
            for(var index=0;index < this.listProveedores.length; index++){
              doc.text(10,altura,this.listProveedores[index].nombre+" ");
              doc.text(60,altura,this.listProveedores[index].telefono+"");
              doc.line(10,alturaLine,200,alturaLine);
              altura+=12;
              alturaLine+=12;
            }
            doc.save('prov.pdf'); 
             
    }
    conductores() {
      let conductor = new jsPDF();
      conductor.text(10,62,"Nombre");
      conductor.text(60,62,"Viaticos Asignados");
      conductor.text(110,62,"Nit");
      conductor.text(160,62,"telefono");
      let altura:number=74;
      let alturaLine:number=78;
      for(var index=0;index < this.listConductores.length; index++){
        conductor.text(10,altura,this.listConductores[index].nombre+" ");
        conductor.text(60,altura,this.listConductores[index].id_recurso.viaticos+"");
        conductor.text(110,altura,this.listConductores[index].nit+" ");
        conductor.text(160,altura,this.listConductores[index].telefono+"");
        conductor.line(10,alturaLine,200,alturaLine);
        altura+=12;
        alturaLine+=12;
      }
      conductor.save('conductores.pdf'); 
       
}
vehiculos() {
  let vehiculo = new jsPDF();
  vehiculo.text(5,62,"# de placa");
  vehiculo.text(35,62,"tarjeta de circulacion");
  vehiculo.text(95,62,"tipo");
  vehiculo.text(135,62,"color");
  vehiculo.text(150,62,"marca");
  
  let altura:number=74;
  let alturaLine:number=78;
  for(var index=0;index < this.listVehiculos.length; index++){
    vehiculo.text(5,altura,this.listVehiculos[index].numero_placa+"");
    vehiculo.text(35,altura,this.listVehiculos[index].tarjeta_circulacion+"");
    vehiculo.text(95,altura,this.listVehiculos[index].tipo+"");
    vehiculo.text(135,altura,this.listVehiculos[index].color+" ");
    vehiculo.text(150,altura,this.listVehiculos[index].marca+"");
    vehiculo.line(130,alturaLine,200,alturaLine);
    altura+=12;
    alturaLine+=12;
  }
  vehiculo.save('vehiculos.pdf'); 
   
}
    getListaDeProveedores():void{
        this.http.get(this.baseURI + "proveedores")
        .subscribe((respuesta:Response) => this.listProveedores = respuesta.json())
      }
      getListaDeConductores():void{
        this.http.get(this.baseURI + "conductores")
        .subscribe((respuesta:Response) => this.listConductores = respuesta.json())
      }
      getListaDeVehiculos():void{
        this.http.get(this.baseURI + "vehiculos")
        .subscribe((respuesta:Response) => this.listVehiculos = respuesta.json())
      }
}
