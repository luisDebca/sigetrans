import { Component, OnInit } from '@angular/core';
import {Http, Response, Headers, RequestOptions}from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {Observer} from 'rxjs/Observer';

@Component({
  selector: 'app-incidentes',
  templateUrl: './incidentes.component.html',
  styleUrls: ['./incidentes.component.css']
})
export class IncidentesComponent implements OnInit {
  baseURI:String = "http://127.0.0.1:8000/sigetrans/";
  listaIncidentes: Array<any>;

  constructor(private http:Http) { }

  ngOnInit() {
    this.getListaIncidentes();
  }

  getListaIncidentes():void{
    this.http.request(this.baseURI + "incidentes")
    .subscribe((respuesta:Response) => this.listaIncidentes = respuesta.json())
  }

}
