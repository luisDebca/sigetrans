import { Component, OnInit } from '@angular/core';
import {Http, Response, Headers, RequestOptions}from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {Observer} from 'rxjs/Observer';
import {AlertCenterService} from '../modules';
import {AlertType} from '../modules';
import {Alert} from '../modules';
import { NgIf } from '@angular/common';

@Component({
  selector: 'app-conductoresregist',
  templateUrl: './conductoresregist.component.html',
  styleUrls: ['./conductoresregist.component.css']
})
export class ConductoresregistComponent implements OnInit {
  baseURI:String = "http://127.0.0.1:8000/sigetrans/";
  conductorItem: any;

  constructor(private http:Http,private service: AlertCenterService) { }

  ngOnInit() {
    this.conductorItem={};
  }

  guardarConductor():void{

  const alert = Alert.create(AlertType.SUCCESS, 'Se guardo correctamente', 4000);
  var headers = new Headers({'Content-Type':'application/json'})
  var options = new RequestOptions({headers: headers});

  var conductor = JSON.stringify(
                  {
                    nombre: this.conductorItem.nombre,
                    nit: this.conductorItem.nit,
                    telefono: this.conductorItem.telefono,
                    id_recurso:
                    {
                        vale_gasolina: this.conductorItem.gasolina,
                        viaticos: this.conductorItem.viaticos
                    }
                  })
      console.info(conductor);
      this.http.post(this.baseURI+"conductor",conductor,options)
      .subscribe((resp:Response)=>{
        this.service.alert(alert);
      });
    }

}
