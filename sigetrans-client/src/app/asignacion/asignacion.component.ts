import { Component, OnInit, Output } from '@angular/core';
import {Http, Response, Headers, RequestOptions}from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {Observer} from 'rxjs/Observer';
import {AlertCenterService} from '../modules';
import {AlertType} from '../modules';
import {Alert} from '../modules';


@Component({
  selector: 'app-asignacion',
  templateUrl: './asignacion.component.html',
  styleUrls: ['./asignacion.component.css']
})
export class AsignacionComponent implements OnInit {
  


  baseURI:String = "http://127.0.0.1:8000/sigetrans/";
  aconductores:Array<any>;
  avehiculos:Array<any>;
  asignacion:Array<any>;
  modeloItem: any;
  showAtt: Boolean;
  constructor(private http:Http,private service: AlertCenterService) { }

  

  ngOnInit() {
    this.modeloItem={};
    this.obtenerTodosConductores();
    this.obtenerTodosVehiculos();
  }
  obtenerTodasAsignaciones():void{
  	this.http.request('http://127.0.0.1:8000/sigetrans/asignaciones')
  	.subscribe((resp:Response)=>{
  		this.asignacion = resp.json();
  	});
  }
  obtenerTodosConductores():void{
  	this.http.request(this.baseURI+"conductores")
  	.subscribe((resp:Response)=>{
  		this.aconductores = resp.json();
  	});
  }
  obtenerTodosVehiculos():void{
  	this.http.request(this.baseURI+"vehiculos")
  	.subscribe((resp:Response)=>{
  		this.avehiculos = resp.json();
  	});
  }

guardarAsigancion():void{
const alert = Alert.create(AlertType.SUCCESS, 'Se guardo correctamente', 5000);        
var headers = new Headers({'Content-Type':'application/json'})
var options = new RequestOptions({headers: headers});

var asignacion = JSON.stringify(
                {  
                  nombre_solicitante:  this.modeloItem.solicitante,
                  carnet_solicitante: this.modeloItem.carnet,
                  id_vehiculo:
                  {
                    id_vehiculo: this.modeloItem.idVehiculo
                  },
                  id_conductor:
                  {
                    id_conductor: this.modeloItem.idConductor
                  },
                  id_ruta:
                  {
                    distancia: this.modeloItem.distancia,
                    inicio: this.modeloItem.inicio,
                    destino: this.modeloItem.destino,
                    departamento: this.modeloItem.departamento,
                    municipio: this.modeloItem.municipio
                  }
                      
})
                 
    this.http.post(this.baseURI+"asignacion",asignacion,options)
    .subscribe((resp:Response)=>{
      this.service.alert(alert);
    });


  }

delete(id):void{
  
this.http.delete('http://127.0.0.1:8000/sigetrans/asignacion/' + id)
.subscribe((resp:Response)=>{
  this.obtenerTodasAsignaciones;


    });

}

}
