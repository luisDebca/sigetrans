import{RouterModule,Routes}from'@angular/router';
import{InicioComponent} from "./inicio/inicio.component";
import{AsignacionComponent} from "./asignacion/asignacion.component";
import {RegistrosComponent} from "./registros/registros.component";
import {MantenimientoComponent} from "./mantenimiento/mantenimiento.component";
import {NuevomantenimientoComponent} from "./nuevomantenimiento/nuevomantenimiento.component";
import {ConductoresComponent} from "./conductores/conductores.component";
import {ReportesComponent} from "./reportes/reportes.component";
import { ProveedoresComponent } from 'app/proveedores/proveedores.component';
import { ConductoresregistComponent } from 'app/conductoresregist/conductoresregist.component';
import { IncidentesComponent } from 'app/incidentes/incidentes.component';
import { IncidentesregistComponent } from 'app/incidentesregist/incidentesregist.component';



const app_routes: Routes = [
 {path: 'inicio', component: InicioComponent },
{path: 'asignacion', component: AsignacionComponent },
{path: 'registros', component: RegistrosComponent},
{path: 'mantenimiento', component: MantenimientoComponent},
{path: 'nuevomantenimiento', component: NuevomantenimientoComponent},
{path: 'conductores', component: ConductoresComponent},
{path: 'reportes', component: ReportesComponent},
{path: 'proveedores', component: ProveedoresComponent},
{path: 'conductoresregist', component: ConductoresregistComponent},
{path: 'incidentes', component: IncidentesComponent},
{path: 'incidentesregist', component: IncidentesregistComponent},
 {path: '**', pathMatch: 'full', redirectTo: 'inicio'}


]

export const app_routing= RouterModule.forRoot(app_routes);
