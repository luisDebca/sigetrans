import { Component, OnInit } from '@angular/core';
import {Http, Response, Headers, RequestOptions}from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {Observer} from 'rxjs/Observer';
import {AlertCenterService} from '../modules';
import {AlertType} from '../modules';
import {Alert} from '../modules';
import { NgIf } from '@angular/common';

@Component({
  selector: 'app-nuevomantenimiento',
  templateUrl: './nuevomantenimiento.component.html',
  styleUrls: ['./nuevomantenimiento.component.css']
})
export class NuevomantenimientoComponent implements OnInit {
  baseURI:String = "http://127.0.0.1:8000/sigetrans/";
  vehiculos:Array<any>;
  proveedores:Array<any>;
  modelItem: any;
  constructor(private http:Http,private service: AlertCenterService) { }

  ngOnInit() {
    this.modelItem={};
    this.getListaDeProveedores();
    this.getListaDeVehiculos();

  }
  getListaDeVehiculos():void{
    this.http.request(this.baseURI + "vehiculos")
    .subscribe((respuesta:Response) => this.vehiculos = respuesta.json())
  }
  getListaDeProveedores():void{
    this.http.request(this.baseURI + "proveedores")
    .subscribe((respuesta:Response) => this.proveedores = respuesta.json())
  }
  guardarMantenimiento():void{
    
  const alert = Alert.create(AlertType.SUCCESS, 'Se guardo correctamente', 4000);      
  var headers = new Headers({'Content-Type':'application/json'})
  var options = new RequestOptions({headers: headers});
  
  var mantenimiento = JSON.stringify(
                  { 
                    diagnostico: this.modelItem.diagnostico,
                    tipo_mantenimiento: this.modelItem.tipoMantenimiento,
                    tipo_revision: this.modelItem.tipoRevision,
                    costo: this.modelItem.costo,
                    periodo_mantenimiento: this.modelItem.periodo,
                    id_proveedor:
                    {
                        id_proveedor: this.modelItem.proveedor
                    }, 
                    id_vehiculo:
                    {
                        id_vehiculo: this.modelItem.vehiculo
                    }
                  })
      console.info(mantenimiento);
      this.http.post(this.baseURI+"mantenimiento",mantenimiento,options)
      .subscribe((resp:Response)=>{
        this.service.alert(alert);
      });
  
  
    }
  
  delete(id):void{
    
  this.http.delete(this.baseURI+"mantenimiento/" + id)
  .subscribe((resp:Response)=>{
    
      });
  
  }

}
