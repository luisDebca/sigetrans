import { Component, OnInit } from '@angular/core';
import {Http, Response, Headers, RequestOptions}from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {Observer} from 'rxjs/Observer';


@Component({
  selector: 'app-registros',
  templateUrl: './registros.component.html',
  styleUrls: ['./registros.component.css']
})
export class RegistrosComponent implements OnInit {

  baseURI:String = "http://127.0.0.1:8000/sigetrans/";
  listaVehiculos: Array<any>;

  constructor(private http:Http) { }

  ngOnInit() {
    this.getListaVehiculos();
  }

  getListaVehiculos():void{
    this.http.request(this.baseURI + "vehiculos")
    .subscribe((respuesta:Response) => this.listaVehiculos = respuesta.json())
  }

}
