import { Component, OnInit, Inject } from '@angular/core';
import {Http, Response, Headers, RequestOptions}from '@angular/http';
@Component({
  selector: 'app-proveedores',
  templateUrl: './proveedores.component.html',
  styleUrls: ['./proveedores.component.css']
})
export class ProveedoresComponent implements OnInit {
  listProveedores:Array<any>;
  baseURI:String = "http://127.0.0.1:8000/sigetrans/";
  constructor(private http:Http) { }

  ngOnInit() {
    this.listaDeProveedores();
  }
  listaDeProveedores():void{
    this.http.get(this.baseURI + "proveedores")
    .subscribe((respuesta:Response) => this.listProveedores = respuesta.json())
  }


}
