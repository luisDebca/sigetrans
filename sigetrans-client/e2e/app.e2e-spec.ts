import { SigetransClientPage } from './app.po';

describe('sigetrans-client App', function() {
  let page: SigetransClientPage;

  beforeEach(() => {
    page = new SigetransClientPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
