SISTEMA PARA LA GESTIÓN DE TRANSPORTE DEL MINISTERIO DE SALUD

El Sistema de Gestión de Transporte permitirá al MINSAL contar con una herramienta para la gestión de las asignaciones de vehículos para las visitas de campo de las diferentes entidades del MINSAL, además de permitir tener un control de los mantenimientos preventivos y correctivos de los vehículos.

Caracteristicas del aplicativo
1. Registro de vehículos
2. Registro de unidades organizacionales
3. Registro de tareas de mantenimiento
4. Registro de mantenimientos realizados
5. Registro de proveedores de mantenimiento
6. Registro de rutas
7. Asignación de conductores a vehículos
8. Registro de variables de estado de cada vehículo
9. Alertas por mantenimiento preventivo
10. Registro de incidentes de tránsito

Ingenieria de software I 2017
